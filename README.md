<h2>Title</h2>

----

Analysis of the terrorist groups modus operandi via the Global Terrorism Database

<h2> Research questions </h2>

----

During the last few years, some terrorist groups have been wielding a lot of power in politically fragile regions, such as the Middle East and the Sub-Saharan Africa. 
By establishing robust strongholds in an extended territory, these groups are able to increase their power by getting access to weapons, intel and also religious or political 
domination. The goal of this project is to study how these patterns can be highlighted by using an extensive database, such as the Global Terrorism Database, developed by the 
National Consortium for the Study of Terrorism and Responses to Terrorism (START). This study focuses on three groups which got a lot of coverage from the occidental media,
namely Boko Haram, Al-Qaeda in the Arabian Peninsula (AQAP) and the Islamic State of Iraq and the Levant (ISIL).

----

<h2> Database </h2>

----

The database can be consulted on [START's official website](https://www.start.umd.edu/gtd/). Please not that, due to a lack of funding from the U.S State Department, the database does not currently 
contain the 2018 dataset.